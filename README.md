=======
Cholesky
======================================

Compilation
--------------------------------------

The code can be compiled by executing the file `Compile`. 

It has a set of commands to do the compilation through the `g++ compiler`.
It is important to notice inside this file that we must use some additional arguments
for the compilation due to the `GSL Library` needed to run the code. So it is fundamental
that the user has both components already installed in their system.


Run Cholesky with an input file
--------------------------------------

An input file is required that contains the line `EndOfData`, preceded by a
list of parameter names and values, one per line, with parameter names and
values separated by a space or tab. If omitted, each parameter will be assigned
a default value. The list of possible parameters and default values, along
with a brief description, can be found in `parameters_dict.cpp`.


Execute the program
--------------------------------------
Once everything is compiled and there were no errors in the compilation process, an executable
called **`Cholesky`** will be created and one can run the code followed by an input file
such as `inputsample`:

    ./Cholesky inputsample
=======
The results expected, which were specified in the inputfile, will be at the **`output`**
folder. These results will be the generated events, along with different analysis such as
the mean energy density, variance, eccentricities, all of those who were chosen by the user.


Visualizing the results
--------------------------------------
In order to have a better view of the output files, one can use `gnuplot` to generate plots from
the folder **`plotsrc`**. If you calculated the mean energy density and you want to see what it
looks like, you can enter said folder and run:

    gnuplot plotmean

The resulting plot will be at the **`results`** directory

