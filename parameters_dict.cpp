##### Parameters_Dictionary #####

//* Cholesky running mode *//

"mode": 1	// 1: Do every step from mode 2 and 3
		
		// 2: Set mean and covariance;
		// Write files with expected mean, variance and correlations; 
		// Do the cholesky decomposition;
		// Store both mean and cholesky decomp in files;

		// 3: Read both files; Generate events respecting the mean and correlation;
		// Calculate the obtained mean, variance, skewness, kurtosis, eccentricities
		// from the generated events, along with correlations.

//* Grid parameters *//

"grid": 10	// Set the grid range of X and Y in fm;
		// X = [ -grid/2 : grid/2 ]; Y = [ -grid/2 : grid/2 ]
"size": 60	// Set the number of cells in the grid.
		// number of cells = size + 1; That way we have the cellsize = grid/size;

//* Events generator *//

"numevents": 10	// Set the number of generated events

//* Outputs *//

// Expected values

"calculate_mean": 0 			// Select whether one wants to calculate the mean (Y=1;N=0)

"calculate_variance": 0			// Select whether one wants to calculate the variance (Y=1;N=0)

"calculate_correlation": 0		// Select whether one wants to calculate the correlation (Y=1;N=0)

"generate_correlation_file": 0		// Select whether one wants to create file with the correlation
					// and correlation as a function of radius (Y=1;N=0);
					// calculate_correlation must be set to 1;
					// size must be even

"generate_surface_correlation_file": 0	// Select whether one wants to create surface visualization of correlation (Y=1;N=0);
					// calculate_correlation must be set to 1;

// Generated events values

"calculate_events_moments": 0		// Select whether one wants to calculate the previous moments
					// mean, variance, correlation, 
					// for the obtained events (Y=1;N=0)

"calculate_skewness": 0			// Select whether one wants to calculate the skewness (Y=1;N=0)

"calculate_kurtosis": 0			// Select whether one wants to calculate the kurtosis (Y=1;N=0)

"calculate_eccentricities": 0		// Select whether one wants to calculate the eccentricities (Y=1;N=0)

//* Probability Distribution Functions *//

				// Select whether one wants to generate random numbers distribution using a

"lognormal_distribution": 0	// lognormal pdf (Y=1;N=0);

"gamma_distribution": 1		// gamma pdf (Y=1;N=0);

"weibull_distribution": 0	// weibull pdf (Y=1;N=0);

"betaprime_distribution": 0	// beta prime pdf (Y=1;N=0);

"pareto_distribution": 0	// pareto pdf (Y=1;N=0);

"nakagima_distribution": 0	// nakagima pdf (Y=1;N=0);

				// the user can select one at a time, otherwise the code will use a gamma pdf;

//* Cutoffs *//

"IRcut": 0.14		// Set the infrared cutoff in GeV [taken from MAGMA (0.14 GeV) [r << 1/m]];

"UVcut": 0.05		// Set the ultraviolet cutoff in fm [cellsize >> rdiv]

"minimum_mean": 50	// Minimum value of energy density mean to be considered for covariance analysis (fm^-4)
                        // Reference: Mean at r = 6.4 fm is 52.92 fm^-4;

//* Impact Parameter *//

"impact_parameter": 0	// Set the Impact parameter in fm


//* EndofData *//

"EndofData"	// All input files must end with "EndofData" in order to read the parameters given. If the inputfile doesn't
		// end with this command, every line below it will be disregarded;

		// If no parameters are given before "EndofData", the program will use the default values specified in this
		// file to run the code and generate events. One does not need to specify every parameter, only the ones they
		// want to have a different value than default.






