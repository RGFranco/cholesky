
#include <iostream>
#include <cstring>
#include "read_in_parameters.h"

using namespace std;

namespace ReadInParameters {

InitData read_in_parameters(std::string input_file) {
    InitData parameter_list;

    // this function reads in parameters
    string tempinput;

    // mode
    int temp_mode = 1;
    tempinput = Util::StringFind4(input_file, "mode");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_mode;
    parameter_list.mode = temp_mode;

    // grid (cells range: -grid/2 to grid/2)
    double temp_grid = 10;
    tempinput = Util::StringFind4(input_file, "grid");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_grid;
    parameter_list.grid = temp_grid;

    // size (number of cells)
    double temp_size = 60;
    tempinput = Util::StringFind4(input_file, "size");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_size;
    parameter_list.size = temp_size;

    // number of events
    double temp_numevents = 10;
    tempinput = Util::StringFind4(input_file, "numevents");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_numevents;
    parameter_list.numevents = temp_numevents;

    // Calculate Mean
    double temp_CalcMean = 0;
    tempinput = Util::StringFind4(input_file, "calculate_mean");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_CalcMean;
    parameter_list.CalcMean = temp_CalcMean;

    // Calculate Variance
    double temp_CalcVar = 0;
    tempinput = Util::StringFind4(input_file, "calculate_variance");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_CalcVar;
    parameter_list.CalcVar = temp_CalcVar;

    // Calculate Correlation
    double temp_CalcCor = 0;
    tempinput = Util::StringFind4(input_file, "calculate_correlation");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_CalcCor;
    parameter_list.CalcCor = temp_CalcCor;

    // Generate file with correlation
    double temp_CorFile = 0;
    tempinput = Util::StringFind4(input_file, "generate_correlation_file");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_CorFile;
    parameter_list.CorFile = temp_CorFile;

    // Generate file with surface of correlation
    double temp_SurfaceCorFile = 0;
    tempinput = Util::StringFind4(input_file, "generate_surface_correlation_file");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_SurfaceCorFile;
    parameter_list.SurfaceCorFile = temp_SurfaceCorFile;

    // Calculate moments on obtained events
    double temp_CalcObtained = 0;
    tempinput = Util::StringFind4(input_file, "calculate_events_moments");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_CalcObtained;
    parameter_list.CalcObtained = temp_CalcObtained;

    // Calculate Skewness
    double temp_CalcSkew = 0;
    tempinput = Util::StringFind4(input_file, "calculate_skewness");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_CalcSkew;
    parameter_list.CalcSkew = temp_CalcSkew;

    // Calculate Kurtosis
    double temp_CalcKurt = 0;
    tempinput = Util::StringFind4(input_file, "calculate_kurtosis");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_CalcKurt;
    parameter_list.CalcKurt = temp_CalcKurt;

    // Calculate Eccentricities
    double temp_CalcEcc = 0;
    tempinput = Util::StringFind4(input_file, "calculate_eccentricities");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_CalcEcc;
    parameter_list.CalcEcc = temp_CalcEcc;

    // Use Lognormal distribution
    double temp_LogNorm = 0;
    tempinput = Util::StringFind4(input_file, "lognormal_distribution");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_LogNorm;
    parameter_list.LogNorm = temp_LogNorm;

    // Use Gamma distribution
    double temp_Gamma = 1;
    tempinput = Util::StringFind4(input_file, "gamma_distribution");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_Gamma;
    parameter_list.Gamma = temp_Gamma;

    // Use Weibull distribution
    double temp_Weibull = 0;
    tempinput = Util::StringFind4(input_file, "weibull_distribution");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_Weibull;
    parameter_list.Weibull = temp_Weibull;

    // Use Beta Prime distribution
    double temp_BetaPrime = 0;
    tempinput = Util::StringFind4(input_file, "betaprime_distribution");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_BetaPrime;
    parameter_list.BetaPrime = temp_BetaPrime;

    // Use Pareto distribution
    double temp_Pareto = 0;
    tempinput = Util::StringFind4(input_file, "pareto_distribution");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_Pareto;
    parameter_list.Pareto = temp_Pareto;

    // Use Nakagima distribution
    double temp_Nakagima = 0;
    tempinput = Util::StringFind4(input_file, "nakagima_distribution");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_Nakagima;
    parameter_list.Nakagima = temp_Nakagima;

    // Infrared Cutoff
    double temp_m = 0.14;
    tempinput = Util::StringFind4(input_file, "IRcut");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_m;
    parameter_list.m = temp_m;

    // Ultraviolet Cutoff
    double temp_rdiv = 0.1;
    tempinput = Util::StringFind4(input_file, "UVcut");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_rdiv;
    parameter_list.rdiv = temp_rdiv;

    // Minimum value of Mean
    double temp_minmean = 50;
    tempinput = Util::StringFind4(input_file, "minimum_mean");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_minmean;
    parameter_list.minMean = temp_minmean;

    // impact parameter
    double temp_b = 0.0;
    tempinput = Util::StringFind4(input_file, "impact_parameter");
    if(tempinput != "empty") istringstream ( tempinput ) >> temp_b;
    parameter_list.b = temp_b;


    cout << "\nDone reading parameters in file " << input_file << endl;
    check_parameters(parameter_list, input_file);

    return parameter_list;
}



void check_parameters(InitData &parameter_list, std::string input_file) {
    cout << "\nChecking input parameter list:" << endl;

    if (parameter_list.mode < 1 || parameter_list.mode > 3) {
        cout << "\033[1;31mERROR\033[0m " << "mode " << parameter_list.mode
                      << " not defined";
        exit(1);
    }

    if (parameter_list.grid < 0) {
        cout << "\033[1;31mERROR\033[0m " << "grid " << parameter_list.grid
                      << " not defined";
        exit(1);
    }

    if (parameter_list.size < 0) {
        cout << "\033[1;31mERROR\033[0m " << "size " << parameter_list.size
                      << " not defined";
        exit(1);
    }

    if (parameter_list.grid/parameter_list.size < 0.1){
        int forw = 1;
        cout << "\033[1;33m(Warning)\033[0m " << "There may be issues with such small cell size of " << parameter_list.grid/parameter_list.size << endl;
        cout << "\nDo you wish to continue? (Y=1;N=0)" << endl;
        cin >> forw;
        if (!forw) exit(1);
    }

    if (parameter_list.numevents < 0) {
        cout << "\033[1;31mERROR\033[0m " << "Number of events " << parameter_list.numevents
                      << " not defined";
        exit(1);
    }

    if (parameter_list.b < 0 || parameter_list.b > 12) {
        cout << "\033[1;31mERROR\033[0m " << "Impact parameter " << parameter_list.b
                      << " not defined";
        exit(1);
    }

    if ((parameter_list.CorFile || parameter_list.SurfaceCorFile) && !parameter_list.CalcCor) {
      cout << "\033[1;33m(Warning)\033[0m " << "In order to generate correlation file and/or surface correlation files, one must calculate the correlations" << endl;
      cout << "Setting calculate_correlation to 1...";
      parameter_list.CalcCor = 1;
      cout << "done" << endl;
    }

    if (parameter_list.CalcObtained && !parameter_list.CalcMean) {
        cout << "\033[1;33m(Warning)\033[0m " << "In order to calculate the obtained moments, one must calculate the mean" << endl;
        cout << "Setting calculate_mean to 1...";
        parameter_list.CalcMean = 1;
        cout << "done" << endl;
    }

    if (parameter_list.LogNorm + parameter_list.Gamma + parameter_list.Weibull + parameter_list.BetaPrime + parameter_list.Pareto + parameter_list.Nakagima != 1){
        cout << "\033[1;33m(Warning)\033[0m " << "Must choose only one probability distribution function" << endl;
        cout << "Setting gamma_distribution to 1...";
        parameter_list.LogNorm = 0;
        parameter_list.Gamma = 1;
        parameter_list.Weibull = 0;
        parameter_list.BetaPrime = 0;
        parameter_list.Pareto = 0;
        parameter_list.Nakagima = 0;
        cout << "done" << endl;
    }

    cout << "Parameters checked!" << endl << "\nEverything looks reasonable so far!" << endl;
}

}
