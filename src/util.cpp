// MUSIC - a 3+1D viscous relativistic hydrodynamic code for heavy ion collisions
// Copyright (C) 2017  Gabriel Denicol, Charles Gale, Sangyong Jeon, Matthew Luzum, Jean-François Paquet, Björn Schenke, Chun Shen

#include "util.h"
#include <iostream>
#include <string>
#include <execinfo.h>

using namespace std;

namespace Util {

int IsFile(string file_name)
{
 FILE *temp;

 if( (temp = fopen(file_name.c_str(),"r")) == NULL) return 0;
 else
  {
   fclose(temp);
   return 1;
  }
}/* IsFile */

// support comments in the parameters file
// comments need to start with #
string StringFind4(string file_name, string str_in) {
    string inputname = file_name;
    string str = str_in;

    string tmpfilename;
    tmpfilename = "input.default";

    // check whether the input parameter file is exist or not
    if (!IsFile(file_name)) {
        if (file_name == "") {
            fprintf(stderr, "No input file name specified.\n");
            fprintf(stderr, "Creating a default file named input.default\n");
        } else {
            cerr << "The file named " << file_name << " is absent." << endl;
            cout << "Creating " << file_name << "..." << endl;
            tmpfilename = file_name;
        }
        ofstream tmp_file(tmpfilename.c_str());
        tmp_file << "EndOfData" << endl;
        tmp_file.close();
        exit(1);
    }/* if isfile */

    // pass checking, now read in the parameter file
    string temp_string;
    ifstream input(inputname.c_str());
    getline(input, temp_string);  // read in the first entry

    int ind = 0;
    string para_name;
    string para_val;
    while (temp_string.compare("EndOfData") != 0) {
        // check whether it is the end of the file
        string para_string;
        stringstream temp_ss(temp_string);
        getline(temp_ss, para_string, '#');  // remove the comments
        if (para_string.compare("") != 0
                && para_string.find_first_not_of(' ') != std::string::npos) {
            // check the read in string is not empty
            stringstream para_stream(para_string);
            para_stream >> para_name >> para_val;
            if (para_name.compare(str) == 0) {
                // find the desired parameter
                ind++;
                input.close();
                return(para_val);
            }  /* if right, return */
        }
        getline(input, temp_string);  // read in the next entry
    }/* while */
    input.close(); // finish read in and close the file

    // the desired parameter is not in the parameter file, then return "empty"
    if (ind == 0) {
        return("empty");
    }
    // should not cross here !!!
    cout << "Error in StringFind4 !!!\n";
    return("empty");
}/* StringFind4 */

}
