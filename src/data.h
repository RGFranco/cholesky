#ifndef SRC_DATA_H_
#define SRC_DATA_H_

#include <array>
#include <stdio.h>
#include <stdlib.h>
#include <string>

//! This is a data structure contains all the parameters for simulation
typedef struct init_data {

    int mode; // 1: do everything;
              // 2: do cholesky decomposition only;
              // 3: do generation of events only;

    // Grid size parameters
    double grid;
    double size;

    // Number of generated events
    double numevents; // Number of events

    // Expected values
    int CalcMean; // Select whether one wants to calculate Mean (Y=1;N=0)
    int CalcVar; // Select whether one wants to calculate Variance (Y=1;N=0)
    int CalcCor; // Select whether one wants to calculate Correlation (Y=1;N=0)
    int CorFile; // (CalcCor must be set to 1) Select whether one wants to create file with Correlation and CorRadius (size must be even) (Y=1;N=0)
    int SurfaceCorFile; // (CalcCor must be set to 1) Select whether one wants to create surface visualization of correlation (Y=1;N=0)
    // Generated Events values
    double CalcObtained; // Select whether wants to calculate the previous moments for the obtained events (Y=1;N=0)
    int CalcSkew; // Select whether one wants to calculate Skewness (Y=1;N=0)
    int CalcKurt; // Select whether one wants to calculate Kurtosis (Y=1;N=0)
    int CalcEcc; // Select whether one wants to calculate Eccentricities (Y=1;N=0)

    // Probability Density Distributions
    int LogNorm; // Select whether one wants to generate random numbers distribution using a lognormal pdf
    int Gamma;  // Select whether one wants to generate random numbers distribution using a gamma pdf
    int Weibull; // Select whether one wants to generate random numbers distribution using a weibull pdf
    int BetaPrime; // Select whether one wants to generate random numbers distribution using a betaprime pdf
    int Pareto; // Select whether one wants to generate random numbers distribution using a pareto pdf
    int Nakagima; // Select whether one wants to generate random numbers distribution using a nakagima pdf

    // Cutoffs
    double m; // infrared cutoff taken from MAGMA (0.14 GeV) in fm-1  [r << 1/m]
    double rdiv; // ultraviolet cutoff in fm  [dx >> rdiv]
    double minMean; // Minimum value of energy density mean to be considered for covariance analysis (fm^-4)
                                 // Reference: Mean at d = 6.4 fm is 52.92 fm-4;
    // Impact Parameter
    double b; // Impact parameter in fm (max = 0.7*grid/2.0)


} InitData;

#endif  // SRC_DATA_H_
