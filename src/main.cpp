#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>
#include <random>
#include <fstream>
#include <complex>
#include "cholesky.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_sf_gamma.h>

using namespace std;

int mode;

double grid,size,x_grid,y_grid,x_size,y_size,numpoints,dx;
double numevents;
int CalcMean,CalcVar,CalcCor,CorFile,SurfaceCorFile,CalcObtained,CalcSkew,CalcKurt,CalcEcc;
int CheckEigen;
int LogNorm,Gamma,Weibull,BetaPrime,Pareto, Nakagima;
double m,rdiv,minMean;
double Ldiv;
double b;

void InitParameters(InitData &DATA);

void InitParameters(InitData &DATA){
  // mode
  mode = DATA.mode;

  // Grid size parameters
  grid = DATA.grid;// = ReadInParameters::DATA.grid;
  size = DATA.size;
  x_grid = grid; // Range of x_grid (max = x_grid/2)
  y_grid = grid; // Range of y_grid (max = y_grid/2)
  x_size = size; // Number of divisions in x
  y_size = size; // Number of divisions in y
  numpoints = (x_size+1)*(y_size+1); // Total number of points
  dx = x_grid/x_size;

  // Number of generated events
  numevents = DATA.numevents; // Number of events

  // Expected values
  CalcMean = DATA.CalcMean; // Select whether one wants to calculate Mean (Y=1;N=0)
  CalcVar = DATA.CalcVar; // Select whether one wants to calculate Variance (Y=1;N=0)
  CalcCor = DATA.CalcCor; // Select whether one wants to calculate Correlation (Y=1;N=0)
  CorFile = DATA.CorFile; // (CalcCor must be set to 1) Select whether one wants to create file with Correlation and CorRadius (size must be even) (Y=1;N=0)
  SurfaceCorFile = DATA.SurfaceCorFile; // (CalcCor must be set to 1) Select whether one wants to create surface visualization of correlation (Y=1;N=0)
  // Generated Events values
  CalcObtained = DATA.CalcObtained; // Select whether wants to calculate the previous moments for the obtained events (Y=1;N=0)
  CalcSkew = DATA.CalcSkew; // Select whether one wants to calculate Skewness (Y=1;N=0)
  CalcKurt = DATA.CalcKurt; // Select whether one wants to calculate Kurtosis (Y=1;N=0)
  CalcEcc = DATA.CalcEcc; // Select whether one wants to calculate Eccentricities (Y=1;N=0)
  CheckEigen = 0; // Select whether one wants to check for negative eigenvalues in covariance matrix (Y=1;N=0)

  // Probability Density Distributions
  LogNorm = DATA.LogNorm; // Select whether one wants to generate random numbers distribution using a lognormal pdf
  Gamma = DATA.Gamma;  // Select whether one wants to generate random numbers distribution using a gamma pdf
  Weibull = DATA.Weibull; // Select whether one wants to generate random numbers distribution using a weibull pdf
  BetaPrime = DATA.BetaPrime; // Select whether one wants to generate random numbers distribution using a betaprime pdf
  Pareto = DATA.Pareto; // Select whether one wants to generate random numbers distribution using a pareto pdf
  Nakagima = DATA.Nakagima; // Select whether one wants to generate random numbers distribution using a nakagima pdf

  // Cutoffs
  m = DATA.m*inv_hbarc; // infrared cutoff taken from MAGMA (0.14 GeV) in fm-1  [r << 1/m]
  rdiv = DATA.rdiv; // ultraviolet cutoff in fm  [dx >> rdiv] (optimal = 0.1)
  minMean = DATA.minMean; // Minimum value of energy density mean to be considered for covariance analysis (fm^-4)
                               // Reference: Mean at d = 6.4 fm is 52.92 fm-4;

  Ldiv = 2 + log(4) + 2.0*log(2.0/(m*rdiv)); //Integral from -rdiv/2 to rdiv/2 divided by 1/rdiv;

  // Impact Parameter
  b = DATA.b; // Impact parameter in fm (max = 0.7*grid/2.0)
}

int main(int argc, char *argv[])
{

    InitData DATA = ReadInParameters::read_in_parameters(argv[1]);

    InitParameters(DATA);

    if (mode != 3)
    {
        cout << "\nCalculating ThicknessFunction:" << endl;
        gsl_vector *Q = ThicknessFunction();
        cout << "done" << endl;

        cout << "\nDefining the Mean of Energy Density:" << endl;
        gsl_vector *MeanP = EnergyDensityMeanInput(Q);     // Initial Mean of the energy density
        cout << "done" << endl;

        cout << "\nSelecting relevant values of Mean for covariance analysis:" << endl;
        int numselected = 0;
        for (int i = 0; i < numpoints; i++)
            if(gsl_vector_get(MeanP,i) > minMean)
                numselected++;
        gsl_vector *iMeanPSelected = gsl_vector_alloc(numselected);
        gsl_vector *MeanPSelected = gsl_vector_alloc(numselected);
        int iselect = 0;
        for (int i = 0; i < numpoints; i++)
        {
            if(gsl_vector_get(MeanP,i) > minMean)
            {
                gsl_vector_set(iMeanPSelected,iselect,i);
                gsl_vector_set(MeanPSelected,iselect,gsl_vector_get(MeanP,i));
                iselect++;
            }
        }
        cout << "done" << endl;

        cout << "\nnumpoints = " << numpoints << endl;
        cout << "numselected = " << numselected << endl;
        cout << "dx = " << dx << endl; //cellsize

        cout << "\nDefining the Covariance Matrix:" << endl;
        gsl_matrix *L = CovarianceEnergyInput(Q,numselected,iMeanPSelected); // Initial Covariance Matrix of the energy density
        cout << "done" << endl;

        cout << "\nDefining the Correlation Matrix and exporting Variance:" << endl;
        gsl_vector *Var = gsl_vector_alloc(numselected);
        for (int i = 0; i < numselected; i++)
        {
          gsl_vector_set(Var,i,gsl_matrix_get(L,i,i));
          gsl_matrix_set(L,i,i,1.0);
        }
        for (int i = 0; i < numselected; i++)
          for (int j = i+1; j < numselected; j++)
          {
              gsl_matrix_set(L,i,j,gsl_matrix_get(L,i,j)/(sqrt(gsl_vector_get(Var,i))*sqrt(gsl_vector_get(Var,j))));
              gsl_matrix_set(L,j,i,gsl_matrix_get(L,i,j));
          }
        cout << "done" << endl;

        if (CheckEigen)
        {
          cout << "\nChecking eigenvalues for positive semi-definite matrix:" << endl;
          gsl_eigen_symm_workspace *w = gsl_eigen_symm_alloc(numselected);
          gsl_vector *VarP = gsl_vector_alloc(numselected);
          for (int i = 0; i < numselected; i++)
            gsl_vector_set(VarP,i,gsl_matrix_get(L,i,i));
          gsl_vector *eval = gsl_vector_alloc(numselected);
          gsl_eigen_symm(L,eval,w);
          int PosDef = 0; // if positive definite, there are no negative eigenvalues in the covariance matrix
          for (int i = 0; i < numselected; i++)
          {
            gsl_matrix_set(L,i,i,gsl_vector_get(VarP,i));
            for (int j = i+1; j < numselected; j++)
              gsl_matrix_set(L,j,i,gsl_matrix_get(L,i,j));
            if(gsl_vector_get(eval,i)<=0)
              PosDef++;
          }
          if (PosDef)
            cout << "Negative eigenvalue(s) found. Matrix not positive definite" << endl;
          else cout << "Matrix is positive definite" << endl;
          gsl_vector_free(eval);
          gsl_vector_free(VarP);
          gsl_eigen_symm_free(w);
        }

        if(CalcMean)
        {
          cout << "\nExporting mean to a file:" << endl;
          ofstream myFileMean;
          myFileMean.open ("./output/ExpectedMean.txt");
          for (int i=0; i<numpoints; i++)
          {
            double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
            double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
            myFileMean << yp << " " << xp << " " << gsl_vector_get(MeanP,i) << endl;
            if (xp == x_grid/2.0)
              myFileMean << "\n";
          }
          myFileMean.close();
          cout << "done" << endl;
        } // If CalcMean

        if(CalcVar)
        {
          cout << "\nStoring variance in File:" << endl;
          ofstream myFileVar;
          myFileVar.open ("./output/ExpectedVar.txt");
          int iselect = 0;
          for (int i=0; i<numpoints; i++)
          {
              double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
              double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
              myFileVar << yp << " " << xp << " ";
              if(i==int(gsl_vector_get(iMeanPSelected,iselect)))
              {
                  myFileVar << gsl_vector_get(Var,iselect) << endl;
                  iselect++;
                  if(iselect==numselected) iselect--;
              }
              else myFileVar << 0.0 << endl;

              if (xp == x_grid/2.0)
                myFileVar << "\n";
          }
          myFileVar.close();
          cout << "done" << endl;
        } // If CalcVar

        if(CalcCor)
        {
            cout << "\nDefining the Correlation Matrix:" << endl;
            gsl_matrix *Cor = gsl_matrix_calloc(numpoints,numpoints);
            for (int k = 0; k < numpoints; k++) gsl_matrix_set(Cor,k,k,1.0);
            for(int iselect = 0; iselect < numselected; iselect++)
            {
                int i = gsl_vector_get(iMeanPSelected,iselect);
                for(int jselect = iselect+1; jselect < numselected; jselect++)
                {
                  int j = gsl_vector_get(iMeanPSelected,jselect);
                  gsl_matrix_set(Cor,i,j,gsl_matrix_get(L,iselect,jselect));
                  gsl_matrix_set(Cor,j,i,gsl_matrix_get(Cor,i,j));
                }
            }
            cout << "done" << endl;

            if (CorFile)
            {
              cout << "\nStoring correlation in File:" << endl;
              ofstream myFileCor;
              myFileCor.open ("./output/ExpectedCor.txt");
              for (int i=0; i<numpoints; i++)
              {
                for (int j=0; j<numpoints; j++)
                  myFileCor << i << " " << j << " " << gsl_matrix_get(Cor,i,j) << endl;
                myFileCor << "\n";
              }
              myFileCor.close();
              cout << "done" << endl;
            } // if CorFile

            int CorRad = 1;
            if(CorRad)
            {
              cout << "\nStoring correlation by radius in File:" << endl;
              ofstream myFileCorRad;
              myFileCorRad.open ("./output/ExpectedCorRadius.txt");
              int nummax = int((size+1)*(size+1)-1);
              for (int i=int(nummax/2); i<numpoints; i = i+int(size+1)){
                double x = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                double y = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                double r = sqrt(x*x + y*y);
                myFileCorRad << r << " " << gsl_matrix_get(Cor,nummax/2,i) << endl;
              }
              myFileCorRad << "\n";
              myFileCorRad.close();
              cout << "done" << endl;
            }

            if(SurfaceCorFile)
            {
              ofstream myFileCenterCorSurf;
              int nummax = int((size+1)*(size+1)-1);
              myFileCenterCorSurf.open ("./output/ExpectedCenterCorSurf.txt");
              for (int i=0; i < numpoints; i++)
              {
                double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                myFileCenterCorSurf << yp << " " << xp << " " << gsl_matrix_get(Cor,nummax/2,i) << endl;
                if (xp == x_grid/2.0)
                  myFileCenterCorSurf << "\n";
              }
              myFileCenterCorSurf << "\n";
              myFileCenterCorSurf.close();

              int j = 0;
              for(int jselect=0; jselect<numselected; jselect++)
              {
                j = gsl_vector_get(iMeanPSelected,jselect);
                double xj = (j%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                double yj = (j/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                if (abs(yj) < dx) break;
              }
              ofstream myFilePeriphCorSurf;
              myFilePeriphCorSurf.open ("./output/ExpectedPeriphCorSurf.txt");
              for (int i=0; i < numpoints; i++)
              {
                double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                myFilePeriphCorSurf << yp << " " << xp << " " << gsl_matrix_get(Cor,j,i) << endl;
                if (xp == x_grid/2.0)
                  myFilePeriphCorSurf << "\n";
              }
              myFilePeriphCorSurf << "\n";
              myFilePeriphCorSurf.close();
            } // if SurfaceCorFile

            gsl_matrix_free(Cor);
        } // if CalcCor

        cout << "\nRunning the Cholesky Decomposition on the Correlation:" << endl;
        gsl_linalg_cholesky_decomp1(L); // Does the Cholesky Decomposition
        for (int i = 0; i < numselected; i++)
          for (int j = i+1; j < numselected; j++)
            gsl_matrix_set(L,i,j,0.0);
        cout << "done" << endl;

        cout << "\nRestoring the Cholesky decomposition on the Covariance:" << endl;
        for (int i = 0; i < numselected; i++)
          for (int j = 0; j < i+1; j++)
          {
            gsl_matrix_set(L,i,j,gsl_matrix_get(L,i,j)*sqrt(gsl_vector_get(Var,i)));
          }
        cout << "done" << endl;

        cout << "\nStoring mean in File:" << endl;
        OutputMeaninFile(MeanP); // Stores the Mean energy density in a file
        cout << "done" << endl;

        cout << "\nStoring Cholesky decomposition in File:" << endl;
        OutputCholeskyinFile(L); // Stores the Decomposition in a file
        cout << "done" << endl;

        gsl_vector_free(Q);
        gsl_vector_free(MeanP);
        gsl_matrix_free(L);
        gsl_vector_free(Var);
        gsl_vector_free(iMeanPSelected);
        gsl_vector_free(MeanPSelected);

        if (mode==2)
            return 0;
    }
    if (mode != 2)
    {
        cout << "\nReading mean File:" << endl;
        gsl_vector *MeanP = InputMeaninFile(numpoints); // Reads the file where the Mean Energy Density is stored
        cout << "done" << endl;

        cout << "\nSelecting relevant values of Mean for covariance analysis:" << endl;
        int numselected = 0;
        for (int i = 0; i < numpoints; i++)
            if(gsl_vector_get(MeanP,i) > minMean)
                numselected++;
        gsl_vector *iMeanPSelected = gsl_vector_alloc(numselected);
        gsl_vector *MeanPSelected = gsl_vector_alloc(numselected);
        int iselect = 0;
        for (int i = 0; i < numpoints; i++)
        {
            if(gsl_vector_get(MeanP,i) > minMean)
            {
                gsl_vector_set(iMeanPSelected,iselect,i);
                gsl_vector_set(MeanPSelected,iselect,gsl_vector_get(MeanP,i));
                iselect++;
            }
        }
        cout << "done" << endl;

        if (mode==3)
        {
          cout << "\nnumpoints = " << numpoints << endl;
          cout << "numselected = " << numselected << endl;
          cout << "dx = " << dx << endl; // cellsize
        }

        cout << "\nReading cholesky File:" << endl;
        gsl_matrix *L = InputCholeskyinFile(numselected,numselected); // Reads the file where the Cholesky Decomposition is stored
        cout << "done" << endl;

        gsl_matrix *X = gsl_matrix_alloc(numselected,numevents);

        // Multiplies inv(L)*<P> to obtain the mean of the points generator <X>
        cout << "\nCounting negative values in inv(L)*<P>:" << endl;
        gsl_blas_dtrsv(CblasLower, CblasNoTrans, CblasNonUnit, L, MeanPSelected);
        // Test if any negative value is present on all events
        int counter = 0;
        for(int iselect=0; iselect < numselected; iselect++)
        {
          if (gsl_vector_get(MeanPSelected,iselect) < 0)
          {
            counter++;
            int i = gsl_vector_get(iMeanPSelected,iselect);
            double xi = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
            double yi = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
            cout << "<X> = " << gsl_vector_get(MeanPSelected,iselect) << " at x = " << xi << " and y = " << yi << " (d = " << sqrt(xi*xi + yi*yi) << ")" << endl;
            gsl_vector_set(MeanPSelected,iselect,0.01);
          }
        }
        cout << "Num of negative numbers: " << counter << endl;
        if (counter) cout << "Set values to 0.01 at those coordinates" << endl;
        counter = 0; // Option to end the program if it expects negative value (Y=1;N=0)
        if (counter)
        {
          gsl_matrix_free(L);
          gsl_vector_free(MeanP);
          gsl_vector_free(iMeanPSelected);
          gsl_vector_free(MeanPSelected);
          gsl_matrix_free(X);
          cout << "\nNegative value for <X>, can't generate distribution" << endl;
          return 0;
        }

        // Random generator using a probability distribution function
        cout << "\nGenerating random uncorrelated values following the ";
        unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // using time as a seed for the generator
        default_random_engine generator (seed);

        if (LogNorm){
          cout << "lognormal distribution:" << endl;
          for (int i=0; i<numselected; i++)
          {
              double MeanX = gsl_vector_get(MeanPSelected,i);
              double Var_MeanX2 = 1/(MeanX*MeanX);
              double m = log(MeanX/sqrt(1+Var_MeanX2));
              double s2 = log(1+Var_MeanX2);
              lognormal_distribution<double> distribution(m,sqrt(s2));
              for (int j=0; j<numevents; j++)
              {
                  gsl_matrix_set(X,i,j,distribution(generator)); // Setting the values of P for each event in the Matrix X
                  distribution.reset();
              }
          }
        }

        if (Gamma){
          cout << "gamma distribution:" << endl;
          for (int i=0; i<numselected; i++)
          {
              double MeanX = gsl_vector_get(MeanPSelected,i);
              double alpha = pow(MeanX,2.0);
              double beta = 1.0/MeanX;
              gamma_distribution<double> distribution(alpha,beta);
              for (int j=0; j<numevents; j++)
              {
                  gsl_matrix_set(X,i,j,distribution(generator)); // Setting the values of P for each event in the Matrix X
                  distribution.reset();
              }
          }
        }

        if (Weibull){
          cout << "weibull distribution:" << endl;
          gsl_vector *K = GetKWeinbull(MeanPSelected,numselected);
          for (int i=0; i<numselected; i++)
          {
            double MeanX = gsl_vector_get(MeanPSelected,i);
            double k = gsl_vector_get(K,i);
            double lambda = MeanX/tgamma(1.0 + 1.0/k);
            weibull_distribution<double> distribution(k,lambda);
            for (int j=0; j<numevents; j++)
            {
                gsl_matrix_set(X,i,j,distribution(generator)); // Setting the values of P for each event in the Matrix X
                distribution.reset();
            }
          }
          gsl_vector_free(K);
        }

        if (BetaPrime){
          cout << "beta prime distribution:" << endl;
          uniform_real_distribution<double> distribution(0.0,1.0);
          double u = 0.0;
          for (int i=0; i<numselected; i++)
          {
              double MeanX = gsl_vector_get(MeanPSelected,i);
              double a = pow(MeanX,3.0) + pow(MeanX,2.0) + MeanX;
              double b = pow(MeanX,2.0) + MeanX + 2.0;
              int interp_size = 1000;
              double dc = 1.0/interp_size;
              gsl_interp_accel *acc = gsl_interp_accel_alloc();
              gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, interp_size+1);
              double c[int(interp_size+1)], U[int(interp_size+1)];
              for (int j = 0; j < interp_size+1; j++)
              {
                c[j] = j*dc;
                U[j] = gsl_sf_beta_inc(a,b,c[j]);
              }
              gsl_spline_init (spline, U, c, interp_size+1);
              for (int j=0; j<numevents; j++)
              {
                u = gsl_spline_eval (spline, distribution(generator), acc);
                gsl_matrix_set(X,i,j,u/(1.0-u));
                distribution.reset();
              }
              gsl_spline_free (spline);
              gsl_interp_accel_free (acc);
          }
        }

        if (Pareto){
          cout << "pareto distribution:" << endl;
          uniform_real_distribution<double> distribution(0.0,1.0);
          double u = 0.0;
          for (int i=0; i<numselected; i++)
          {
            double MeanX = gsl_vector_get(MeanPSelected,i);
            double alpha = 1.0 + sqrt(1.0 + pow(MeanX,2.0));
            double lambda = (alpha-1.0)*MeanX/alpha;
            for (int j=0; j<numevents; j++)
            {
              u = lambda/pow(1.0-distribution(generator),1.0/alpha);
              gsl_matrix_set(X,i,j,u);
              distribution.reset();
            }
          }
        }

        if (Nakagima){
          cout << "nakagima distribution:" << endl;
          uniform_real_distribution<double> distribution(0.0,0.999);
          double u = 0.0;
          double min = gsl_vector_get(MeanPSelected,0);
          double max = min;
          double MeanX = min;
          for (int i=0; i<numselected; i++)
          {
              MeanX = gsl_vector_get(MeanPSelected,i);
              if (MeanX > max) max = MeanX;
              if (MeanX < min) min = MeanX;
          }
          int interp_size = 1000;
          cout << "min = " << min << endl;
          cout << "max = " << max << endl;
          if (min < 0.525){
            cout << "\033[1;31mERROR\033[0m " << "minimum expected value less than possible for interpolation" << endl;
            return 0;
          }
          double dm = 5.0/interp_size;
          gsl_interp_accel *acc1 = gsl_interp_accel_alloc();
          gsl_spline *spline1 = gsl_spline_alloc(gsl_interp_cspline, interp_size+1);
          double mi[int(interp_size+1)], MeanXi[int(interp_size+1)];
          for (int j = 0; j < interp_size+1; j++)
          {
            mi[j] = 0.06 + j*dm;
            MeanXi[j] = tgamma(mi[j] + 1.0/2.0)/(tgamma(mi[j])*sqrt(mi[j]));
          }
          gsl_spline_init (spline1, MeanXi, mi, interp_size+1);

          for (int i=0; i<numselected; i++)
          {
              MeanX = gsl_vector_get(MeanPSelected,i);
              double a = gsl_spline_eval (spline1, MeanX/sqrt(1.0 + pow(MeanX,2.0)), acc1);
              double b = 1.0 + pow(MeanX,2.0);

              double dc = 10.0/interp_size;
              gsl_interp_accel *acc = gsl_interp_accel_alloc();
              gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, interp_size+1);
              double c[int(interp_size+1)], U[int(interp_size+1)];
              for (int j = 0; j < interp_size+1; j++)
              {
                c[j] = j*dc;
                U[j] = gsl_sf_gamma_inc_P(a,(a/b)*c[j]*c[j]);
              }
              gsl_spline_init (spline, U, c, interp_size+1);
              for (int j=0; j<numevents; j++)
              {
                u = gsl_spline_eval (spline, distribution(generator), acc);
                gsl_matrix_set(X,i,j,u);
                distribution.reset();
              }
              gsl_spline_free (spline);
              gsl_interp_accel_free (acc);
          }
          gsl_spline_free (spline1);
          gsl_interp_accel_free (acc1);
        }

        cout << "done" << endl; // Generating random uncorrelated...

        // Multiplies L*X to obtain the values of P for each event
        cout << "\nGenerating correlation between the values:" << endl;
        double alpha = 1.0;
        gsl_blas_dtrmm(CblasLeft, CblasLower, CblasNoTrans, CblasNonUnit, alpha, L, X);
        cout << "done" << endl;

        // Test if any negative value is present on all events
        cout << "\nSearching negative values in each event:" << endl;
        counter = 0;
        double Xmax = 0;
        double jmax, xmax, ymax;
        for(int j = 0; j < numevents; j++)
          for(int iselect = 0; iselect < numselected; iselect++)
          {
            if (gsl_matrix_get(X,iselect,j) < 0)
            {
              counter++;
              int i = gsl_vector_get(iMeanPSelected,iselect);
              double x0 = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
              double y0 = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
              double d0 = sqrt(x0*x0+y0*y0);
              cout << "Negative value: " << gsl_matrix_get(X,iselect,j) << " at d = " << d0 << " at event " << j+1 << endl;
              gsl_matrix_set(X,iselect,j,0);
            }
            if (gsl_matrix_get(X,iselect,j) > Xmax)
            {
              int i = gsl_vector_get(iMeanPSelected,iselect);
              xmax = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
              ymax = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
              Xmax = gsl_matrix_get(X,iselect,j);
              jmax = j;
            }
          }
        cout << "Num of negative values: " << counter << endl;
        cout << "\nMaximum value of energy density: " << Xmax << " at event " << jmax+1 << " at y = " << ymax << " x = " << xmax << endl;
        counter = 0;

        if(CalcObtained)
        {
          // Test to see if <P> matches with MeanP
          if (CalcMean)
          {
              cout << "\nCalculating obtained mean:" << endl;
              gsl_vector *MeanPTest = gsl_vector_calloc(numpoints);
              int iselect = 0;
              for (int i=0; i<numpoints; i++)
              {
                  if (i==gsl_vector_get(iMeanPSelected,iselect))
                  {
                      for (int k = 0; k < numevents; k++)
                          gsl_vector_set(MeanPTest,i,gsl_vector_get(MeanPTest,i) + (gsl_matrix_get(X,iselect,k)/numevents));
                      iselect++;
                      if(iselect==numselected) iselect--;
                  }
                  else
                      gsl_vector_set(MeanPTest,i,gsl_vector_get(MeanP,i));
              }
              cout << "done" << endl;

              cout << "\nGenerating file with obtained mean:" << endl;
              ofstream myFileCalcMean;
              myFileCalcMean.open ("./output/CalculatedMean.txt");
              for (int i=0; i<numpoints; i++)
              {
                double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                myFileCalcMean << yp << " " << xp << " " << gsl_vector_get(MeanPTest,i) << endl;
                if (xp == x_grid/2.0)
                  myFileCalcMean << "\n";
              }
              myFileCalcMean.close();
              cout << "done" << endl;

              if (CalcVar)
              {
                cout << "\nCalculating obtained variance:" << endl;
                gsl_vector *VarPTest = gsl_vector_calloc(numpoints);
                for(iselect = 0; iselect < numselected; iselect++)
                {
                  int i = gsl_vector_get(iMeanPSelected,iselect);
                  for (int k = 0; k < numevents; k++)
                    gsl_vector_set(VarPTest,i,gsl_vector_get(VarPTest,i) + pow(gsl_matrix_get(X,iselect,k)-gsl_vector_get(MeanPTest,i),2.0)/numevents);
                }
                cout << "done" << endl;

                cout << "\nGenerating file with obtained variance:" << endl;
                ofstream myFileCalcVar;
                myFileCalcVar.open ("./output/CalculatedVar.txt");
                for (int i=0; i<numpoints; i++)
                {
                  double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                  double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                  myFileCalcVar << yp << " " << xp << " " << gsl_vector_get(VarPTest,i) << endl;
                  if (xp == x_grid/2.0)
                    myFileCalcVar << "\n";
                }
                myFileCalcVar.close();
                cout << "done" << endl;
                gsl_vector_free(VarPTest);

              } // If CalcVar

              if (CalcCor)
              {
                  // Test to see if Cov(P) matches with CovarianceEnergyInput
                  cout << "\nCalculating obtained correlation:" << endl;
                  gsl_matrix *CorPTest = gsl_matrix_calloc(numpoints,numpoints);
                  for (iselect=0; iselect < numselected; iselect++)
                  {
                    int i = gsl_vector_get(iMeanPSelected,iselect);
                    for (int jselect=iselect; jselect < numselected; jselect++)
                    {
                      int j = gsl_vector_get(iMeanPSelected,jselect);
                      for (int k=0; k < numevents; k++)
                        gsl_matrix_set(CorPTest,i,j,gsl_matrix_get(CorPTest,i,j) + ((gsl_matrix_get(X,iselect,k)-gsl_vector_get(MeanPTest,i))*(gsl_matrix_get(X,jselect,k)-gsl_vector_get(MeanPTest,j)))/numevents);
                    }
                  }
                  for (int iselect=0; iselect < numselected; iselect++)
                  {
                    int i = gsl_vector_get(iMeanPSelected,iselect);
                    for (int jselect=iselect+1; jselect < numselected; jselect++)
                    {
                      int j = gsl_vector_get(iMeanPSelected,jselect);
                      gsl_matrix_set(CorPTest,i,j,gsl_matrix_get(CorPTest,i,j)/(sqrt(gsl_matrix_get(CorPTest,i,i))*sqrt(gsl_matrix_get(CorPTest,j,j))));
                      gsl_matrix_set(CorPTest,j,i,gsl_matrix_get(CorPTest,i,j));
                    }
                  }
                  for (int iselect=0; iselect < numselected; iselect++)
                  {
                    int i=gsl_vector_get(iMeanPSelected,iselect);
                    gsl_matrix_set(CorPTest,i,i,1.0);
                  }
                  cout << "done" << endl;

                  if(CorFile)
                  {
                    cout << "\nGenerating file with obtained correlation:" << endl;
                    ofstream myFileCalcCor;
                    myFileCalcCor.open ("./output/CalculatedCor.txt");
                    for (int i=0; i<numpoints; i++)
                    {
                      for (int j=0; j<numpoints; j++)
                        myFileCalcCor << i << " " << j << " " << gsl_matrix_get(CorPTest,i,j) << endl;
                      myFileCalcCor << "\n";
                    }
                    myFileCalcCor.close();
                    cout << "done" << endl;
                  } //if CorFile

                  int CorRad = 1;
                  if (CorRad)
                  {
                    cout << "\nGenerating file with obtained correlation radius:" << endl;
                    ofstream myFileCalcCorRad;
                    myFileCalcCorRad.open ("./output/CalculatedCorRadius.txt");
                    int nummax = int((size+1)*(size+1)-1);
                    for (int i=int(nummax/2); i<numpoints; i = i+int(size+1))
                    {
                      double x = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                      double y = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                      double r = sqrt(x*x + y*y);
                      double CorMean = (gsl_matrix_get(CorPTest,nummax/2,i)+gsl_matrix_get(CorPTest,i,nummax/2)+gsl_matrix_get(CorPTest,nummax/2,nummax-i)+gsl_matrix_get(CorPTest,nummax-i,nummax/2))/4.0;
                      myFileCalcCorRad << r << " " << CorMean << endl;
                    }
                    myFileCalcCorRad << "\n";
                    myFileCalcCorRad.close();
                    cout << "done" << endl;
                  }

                  if(SurfaceCorFile)
                  {
                    ofstream myFileCenterCorSurfCalc;
                    int nummax = int((size+1)*(size+1)-1);
                    myFileCenterCorSurfCalc.open ("./output/ExpectedCenterCorSurfCalc.txt");
                    for (int i=0; i < numpoints; i++)
                    {
                      double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                      double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                      myFileCenterCorSurfCalc << yp << " " << xp << " " << gsl_matrix_get(CorPTest,nummax/2,i) << endl;
                      if (xp == x_grid/2.0)
                        myFileCenterCorSurfCalc << "\n";
                    }
                    myFileCenterCorSurfCalc << "\n";
                    myFileCenterCorSurfCalc.close();

                    int j = 0;
                    for(int jselect=0; jselect<numselected; jselect++)
                    {
                      j = gsl_vector_get(iMeanPSelected,jselect);
                      double xj = (j%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                      double yj = (j/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                      if (abs(yj) < dx) break;
                    }
                    ofstream myFilePeriphCorSurfCalc;
                    myFilePeriphCorSurfCalc.open ("./output/ExpectedPeriphCorSurfCalc.txt");
                    for (int i=0; i < numpoints; i++)
                    {
                      double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                      double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                      myFilePeriphCorSurfCalc << yp << " " << xp << " " << gsl_matrix_get(CorPTest,j,i) << endl;
                      if (xp == x_grid/2.0)
                        myFilePeriphCorSurfCalc << "\n";
                    }
                    myFilePeriphCorSurfCalc << "\n";
                    myFilePeriphCorSurfCalc.close();
                  }
                  gsl_matrix_free(CorPTest);
               } // If CalcCor

               if(CalcSkew)
               {
                 cout << "\nCalculating expected skewness:" << endl;
                 gsl_vector *SkewP = gsl_vector_calloc(numselected);
                 gsl_vector *SkewPExp = gsl_vector_calloc(numpoints);
                 gsl_vector *K = GetKWeinbull(MeanPSelected,numselected);
                 for(iselect = 0; iselect < numselected; iselect++)
                 {
                   double MeanX = gsl_vector_get(MeanPSelected,iselect);

                   if (Gamma) gsl_vector_set(SkewP,iselect,2.0/MeanX);
                   if (LogNorm) gsl_vector_set(SkewP,iselect,(1.0+3.0*pow(MeanX,2.0))/pow(MeanX,3.0));
                   if (BetaPrime)
                   {
                     double a = pow(MeanX,3.0) + pow(MeanX,2.0) + MeanX;
                     double b = pow(MeanX,2.0) + MeanX + 2.0;
                     if(b < 3.0){
                       cout << "\033[1;31mERROR\033[0m " << "Cannot calculate skewness because parameter b < 3" << endl;
                       break;
                     }
                     gsl_vector_set(SkewP,iselect,(2.0*(2.0*a+b-1.0)/(b-3))*sqrt((b-2.0)/(a*(a+b-1.0))));
                   }
                   if (Weibull)
                   {
                     double k = gsl_vector_get(K,iselect);
                     double lambda = MeanX/tgamma(1.0 + 1.0/k);
                     gsl_vector_set(SkewP,iselect,tgamma(1.0 + 3.0/k)*pow(lambda,3.0) - 3.0*MeanX - pow(MeanX,3.0));
                   }
                   int i = gsl_vector_get(iMeanPSelected,iselect);
                   double var2 = 0.0;
                   for(int jselect = 0; jselect < iselect+1; jselect++)
                     var2 += pow(gsl_matrix_get(L,iselect,jselect),2.0);
                   for(int jselect = 0; jselect < iselect+1; jselect++)
                     gsl_vector_set(SkewPExp,i,gsl_vector_get(SkewPExp,i) + pow(gsl_matrix_get(L,iselect,jselect),3.0)*gsl_vector_get(SkewP,jselect)/pow(var2,3.0/2.0));
                 }
                 gsl_vector_free(K);
                 cout << "done" << endl;

                 cout << "\nGenerating file with expected skewness:" << endl;
                 ofstream myFileExpSkew;
                 myFileExpSkew.open ("./output/ExpectedSkew.txt");
                 for (int i=0; i<numpoints; i++)
                 {
                   double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                   double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                   myFileExpSkew << yp << " " << xp << " " << gsl_vector_get(SkewPExp,i) << endl;
                   if (xp == x_grid/2.0)
                     myFileExpSkew << "\n";
                 }
                 myFileExpSkew.close();
                 cout << "done" << endl;
                 gsl_vector_free(SkewP);
                 gsl_vector_free(SkewPExp);

                 cout << "\nCalculating obtained skewness:" << endl;
                 gsl_vector *SkewPTest = gsl_vector_calloc(numpoints);
                 gsl_vector *VarPTest = gsl_vector_calloc(numpoints);
                 for(iselect = 0; iselect < numselected; iselect++)
                 {
                   int i = gsl_vector_get(iMeanPSelected,iselect);
                   for (int k = 0; k < numevents; k++)
                   {
                     gsl_vector_set(SkewPTest,i,gsl_vector_get(SkewPTest,i) + pow(gsl_matrix_get(X,iselect,k)-gsl_vector_get(MeanPTest,i),3.0)/numevents);
                     gsl_vector_set(VarPTest,i,gsl_vector_get(VarPTest,i) + pow(gsl_matrix_get(X,iselect,k)-gsl_vector_get(MeanPTest,i),2.0)/numevents);
                   }
                 }
                 cout << "done" << endl;

                 cout << "\nGenerating file with obtained skewness:" << endl;
                 ofstream myFileCalcSkew;
                 myFileCalcSkew.open ("./output/CalculatedSkew.txt");
                 for (int i=0; i<numpoints; i++)
                 {
                   double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                   double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                   if(gsl_vector_get(VarPTest,i)) myFileCalcSkew << yp << " " << xp << " " << gsl_vector_get(SkewPTest,i)/pow(gsl_vector_get(VarPTest,i),1.5) << endl;
                   else myFileCalcSkew << yp << " " << xp << " " << 0 << endl;
                   if (xp == x_grid/2.0)
                     myFileCalcSkew << "\n";
                 }
                 myFileCalcSkew.close();
                 cout << "done" << endl;
                 gsl_vector_free(SkewPTest);
                 gsl_vector_free(VarPTest);
               } // If CalcSkew

               if(CalcKurt)
               {
                 cout << "\nCalculating expected kurtosis:" << endl;
                 gsl_vector *KurtP = gsl_vector_calloc(numselected);
                 gsl_vector *KurtPExp = gsl_vector_calloc(numpoints);
                 gsl_vector *K = GetKWeinbull(MeanPSelected,numselected);
                 for(iselect = 0; iselect < numselected; iselect++)
                 {
                   double MeanX = gsl_vector_get(MeanPSelected,iselect);

                   if (Gamma) gsl_vector_set(KurtP,iselect,6.0/pow(MeanX,2.0));
                   if (LogNorm)
                   {
                     double m2 = 1.0/pow(MeanX,2.0);
                     gsl_vector_set(KurtP,iselect,16.0*m2 + 150*pow(m2,2.0) + 6.0*pow(m2,3.0) + pow(m2,4.0));
                   }
                   if (BetaPrime)
                   {
                     double a = pow(MeanX,3.0) + pow(MeanX,2.0) + MeanX;
                     double b = pow(MeanX,2.0) + MeanX + 2.0;
                     if(b < 4.0){
                       cout << "\033[1;31mERROR\033[0m " << "Cannot calculate kurtosis because parameter b < 4" << endl;
                       break;
                     }
                     gsl_vector_set(KurtP,iselect,6.0*(a*(a+b-1.0)*(5.0*b-11.0)+(b-1.0)*(b-1.0)*(b-2.0))/(a*(a+b-1.0)*(b-3.0)*(b-4.0)));
                   }
                   if (Weibull)
                   {
                     double k = gsl_vector_get(K,iselect);
                     double lambda = MeanX/tgamma(1.0 + 1.0/k);
                     gsl_vector_set(KurtP,iselect,pow(lambda,4.0)*tgamma(1.0+4.0/k) - 4.0*(tgamma(1.0 + 3.0/k)*pow(lambda,3.0) - 3.0*MeanX - pow(MeanX,3.0))*MeanX - 6*pow(MeanX,2.0) - pow(MeanX,4.0) - 3.0);
                   }
                   int i = gsl_vector_get(iMeanPSelected,iselect);
                   double var2 = 0.0;
                   for(int jselect = 0; jselect < iselect+1; jselect++)
                     var2 += pow(gsl_matrix_get(L,iselect,jselect),2.0);
                   for(int jselect = 0; jselect < iselect+1; jselect++)
                     gsl_vector_set(KurtPExp,i,gsl_vector_get(KurtPExp,i) + pow(gsl_matrix_get(L,iselect,jselect),4.0)*gsl_vector_get(KurtP,jselect)/pow(var2,2.0));
                 }
                 gsl_vector_free(K);
                 cout << "done" << endl;

                 cout << "\nGenerating file with expected kurtosis:" << endl;
                 ofstream myFileExpKurt;
                 myFileExpKurt.open ("./output/ExpectedKurt.txt");
                 for (int i=0; i<numpoints; i++)
                 {
                   double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                   double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                   myFileExpKurt << yp << " " << xp << " " << gsl_vector_get(KurtPExp,i) << endl;
                   if (xp == x_grid/2.0)
                     myFileExpKurt << "\n";
                 }
                 myFileExpKurt.close();
                 cout << "done" << endl;
                 gsl_vector_free(KurtP);
                 gsl_vector_free(KurtPExp);

                 cout << "\nCalculating obtained kurtosis:" << endl;
                 gsl_vector *KurtPTest = gsl_vector_calloc(numpoints);
                 gsl_vector *VarPTest = gsl_vector_calloc(numpoints);
                 for(iselect = 0; iselect < numselected; iselect++)
                 {
                   int i = gsl_vector_get(iMeanPSelected,iselect);
                   for (int k = 0; k < numevents; k++)
                   {
                     gsl_vector_set(KurtPTest,i,gsl_vector_get(KurtPTest,i) + pow(gsl_matrix_get(X,iselect,k)-gsl_vector_get(MeanPTest,i),4.0)/numevents);
                     gsl_vector_set(VarPTest,i,gsl_vector_get(VarPTest,i) + pow(gsl_matrix_get(X,iselect,k)-gsl_vector_get(MeanPTest,i),2.0)/numevents);
                   }
                 }
                 cout << "done" << endl;

                 cout << "\nGenerating file with obtained kurtosis:" << endl;
                 ofstream myFileCalcKurt;
                 myFileCalcKurt.open ("./output/CalculatedKurt.txt");
                 for (int i=0; i<numpoints; i++)
                 {
                   double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
                   double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
                   if(gsl_vector_get(VarPTest,i)) myFileCalcKurt << yp << " " << xp << " " << gsl_vector_get(KurtPTest,i)/pow(gsl_vector_get(VarPTest,i),2.0) - 3 << endl;
                   else myFileCalcKurt << yp << " " << xp << " " << 0 << endl;
                   if (xp == x_grid/2.0)
                     myFileCalcKurt << "\n";
                 }
                 myFileCalcKurt.close();
                 cout << "done" << endl;
                 gsl_vector_free(KurtPTest);
                 gsl_vector_free(VarPTest);
               } // If CalcKurt
               gsl_vector_free(MeanPTest);
          } // if CalcMean
        } // if CalcObtained

        if (!counter)
        {
          cout << "\nInitial Energy Density generated successfully!" << endl;

          // Output energy density to a file
          cout << "\nGenerating file with events: " << endl;
          ofstream myFile;
          myFile.open ("./output/EnergyDensity.txt");
          int iselect = 0;
          for (int i=0; i<numpoints; i++)
          {
              double xp = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
              double yp = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
              myFile << yp << " " << xp << " ";
              if(i==int(gsl_vector_get(iMeanPSelected,iselect)))
              {
                  for (int j=0; j<numevents; j++)
                      myFile << gsl_matrix_get(X,iselect,j) << " ";
                  myFile << endl;
                  iselect++;
                  if(iselect==numselected) iselect--;
              }
              else
              {
                  for (int j=0; j<numevents; j++)
                      myFile << gsl_vector_get(MeanP,i) << " ";
                  myFile << endl;
              }
              if (xp == x_grid/2.0)
                myFile << "\n";
          }
          myFile.close();
          cout << "done" << endl;
        }

        if (CalcEcc)
        {
          cout << "\nCalculating eccentricities and generating file:" << endl;
          int zmax = 6;
          ofstream myFileEcc;
          myFileEcc.open("./output/Eccentricities.txt");
          myFileEcc << "#TotalEnergy \t W11 \t Ecc_2 \t Ecc_3 \t Ecc_4 \t Ecc_5\n" << endl;
          double MeanEcc_2 = 0.0;
          double MeanEcc_3 = 0.0;
          double MeanEcc_4 = 0.0;
          double MeanEcc_5 = 0.0;
          double SquaredEcc_2 = 0.0;
          double SquaredEcc_3 = 0.0;
          double SquaredEcc_4 = 0.0;
          double SquaredEcc_5 = 0.0;
          for (int j=0; j<numevents+1; j++)
          {
            complex<double> eps[zmax][zmax] = {{0}}; // moment <z^j z*^k> =  <r^(j+k) e^{i(j-k) phi}>
            int iselect = 0;
            for (int i=0; i<numpoints ; i++)
            {
              if(i==int(gsl_vector_get(iMeanPSelected,iselect)))
              {
                if (j<numevents) eps[0][0] += gsl_matrix_get(X,iselect,j);
                else eps[0][0] += gsl_vector_get(MeanP,i);
                iselect++;
                if(iselect==numselected) iselect--;
              }
            }
            iselect = 0;
            for (int i=0; i<numpoints; i++)
            {
              double x = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
              double y = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
              complex<double> z (x,y);
              complex<double> zbar = conj(z);
              for(int m=0; m<zmax; m++)
                for(int n=0; n<zmax; n++)
                  if(!(m==0 && n==0))
                  {
                    complex<double> powz, powzbar;
                    if(abs(z) == 0.0) // pow() doesn't work nicely with a vanishing complex number
                    {
                      powz = pow(0,m);
                      powzbar = pow(0,n);
                    }
                    else
                    {
                      powz = pow(z,m);
                      powzbar = pow(zbar,n);
                    }
                    if(i==int(gsl_vector_get(iMeanPSelected,iselect)))
                    {
                      if (j<numevents) eps[m][n] += gsl_matrix_get(X,iselect,j)*powz*powzbar/eps[0][0];
                      else eps[m][n] += gsl_vector_get(MeanP,i)*powz*powzbar/eps[0][0];
                    }
                  }
              if(i==int(gsl_vector_get(iMeanPSelected,iselect)))
              {
                iselect++;
                if(iselect==numselected) iselect--;
              }
            }

            double TotalE = eps[0][0].real()*dx*dx; // Total energy
            complex<double> W11 = eps[1][0]; // W_11
            complex<double> Ecc_2 = -(eps[2][0]-eps[1][0]*eps[1][0])/(eps[1][1] - 2.0*eps[1][0]*eps[0][1]); // Ecc_2
            complex<double> Ecc_3 = -(eps[3][0] + eps[1][0]*(3.0*eps[2][0] - 2.0*eps[1][0]*eps[1][0]))/pow(eps[1][1] - 2.0*eps[1][0]*eps[0][1],1.5); // Ecc_3
            complex<double> Ecc_4 = -(eps[4][0] - 4.0*eps[3][0]*eps[1][0] - 3.0*pow(eps[2][0],2.0) + 12.0*eps[2][0]*pow(eps[1][0],2.0) - 6.0*pow(eps[1][0],4.0))/pow(eps[1][1] - 2.0*eps[1][0]*eps[0][1],2.0); // Ecc_4
            complex<double> Ecc_5 = -(eps[5][0] + 20.0*eps[3][0]*pow(eps[1][0],2.0) - 60.0*eps[2][0]*pow(eps[1][0],3.0) + 5.0*(6.0*pow(eps[2][0],2.0) - eps[4][0])*eps[1][0] - 10.0*eps[2][0]*eps[3][0] + 24.0*pow(eps[1][0],5.0))/pow(eps[1][1] - 2.0*eps[1][0]*eps[0][1],2.5); // Ecc_5
            if (j<numevents)
            {
              myFileEcc << TotalE << "\t\t" << W11 << "\t" << Ecc_2 << "\t" << Ecc_3 << "\t" << Ecc_4 << "\t" << Ecc_5 << endl;

              MeanEcc_2 += sqrt(pow(Ecc_2.real(),2.0) + pow(Ecc_2.imag(),2.0))/numevents;
              MeanEcc_3 += sqrt(pow(Ecc_3.real(),2.0) + pow(Ecc_3.imag(),2.0))/numevents;
              MeanEcc_4 += sqrt(pow(Ecc_4.real(),2.0) + pow(Ecc_4.imag(),2.0))/numevents;
              MeanEcc_5 += sqrt(pow(Ecc_5.real(),2.0) + pow(Ecc_5.imag(),2.0))/numevents;

              SquaredEcc_2 += (pow(Ecc_2.real(),2.0) + pow(Ecc_2.imag(),2.0))/numevents;
              SquaredEcc_3 += (pow(Ecc_3.real(),2.0) + pow(Ecc_3.imag(),2.0))/numevents;
              SquaredEcc_4 += (pow(Ecc_4.real(),2.0) + pow(Ecc_4.imag(),2.0))/numevents;
              SquaredEcc_5 += (pow(Ecc_5.real(),2.0) + pow(Ecc_5.imag(),2.0))/numevents;
            }
            else
            {
              myFileEcc << "\n\n#Expected TotalE \t W11 \t Ecc_2 \t Ecc_3 \t Ecc_4 \t Ecc_5\n" << endl;
              myFileEcc << TotalE << " " << W11 << " " << Ecc_2 << " " << Ecc_3 << " " << Ecc_4 << " " << Ecc_5 << endl;
            }
          }
          myFileEcc << "\n#Mean -> Ecc_2 \t Ecc_3\t Ecc_4\t Ecc_5\n" << endl;
          myFileEcc << "      -> " << MeanEcc_2 << " " << MeanEcc_3 << " " << MeanEcc_4 << " " << MeanEcc_5 << endl;

          myFileEcc << "\n#SqrtMean -> SqrtEcc_2 \t SqrtEcc_3\t SqrtEcc_4\t SqrtEcc_5\n" << endl;
          myFileEcc << "      -> " << sqrt(SquaredEcc_2) << " " << sqrt(SquaredEcc_3) << " " << sqrt(SquaredEcc_4) << " " << sqrt(SquaredEcc_5) << endl;

          myFileEcc << "\n#Ratio values for RHIC and LHC\n" << endl;
          myFileEcc << "      -> " << sqrt(SquaredEcc_2)/pow(sqrt(SquaredEcc_3),0.5) << " " << sqrt(SquaredEcc_2)/pow(sqrt(SquaredEcc_3),0.6) <<  endl;
          myFileEcc.close();
          cout << "done" << endl;
        }

        gsl_matrix_free(L);
        gsl_vector_free(MeanP);
        gsl_vector_free(iMeanPSelected);
        gsl_vector_free(MeanPSelected);
        gsl_matrix_free(X);

    } // If mode!=2
    return 0;
}
