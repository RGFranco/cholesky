#ifndef CHOLESKY_H_INCLUDED
#define CHOLESKY_H_INCLUDED
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_eigen.h>
#include "read_in_parameters.h"
#include "data.h"
#include "util.h"

using namespace std;

// mode
extern int mode;

// Grid size parameters
extern double grid;// = ReadInParameters::DATA.grid;
extern double size;
extern double x_grid; // Range of x_grid (max = x_grid/2)
extern double y_grid; // Range of y_grid (max = y_grid/2)
extern double x_size; // Number of divisions in x
extern double y_size; // Number of divisions in y
extern double numpoints; // Total number of points
extern double dx;

// Number of generated events
extern double numevents; // Number of events

// Expected values
extern int CalcMean; // Select whether one wants to calculate Mean (Y=1;N=0)
extern int CalcVar; // Select whether one wants to calculate Variance (Y=1;N=0)
extern int CalcCor; // Select whether one wants to calculate Correlation (Y=1;N=0)
extern int CorFile; // (CalcCor must be set to 1) Select whether one wants to create file with Correlation and CorRadius (size must be even) (Y=1;N=0)
extern int SurfaceCorFile; // (CalcCor must be set to 1) Select whether one wants to create surface visualization of correlation (Y=1;N=0)
// Generated Events values
extern int CalcObtained; // Select whether wants to calculate the previous moments for the obtained events (Y=1;N=0)
extern int CalcSkew; // Select whether one wants to calculate Skewness (Y=1;N=0)
extern int CalcKurt; // Select whether one wants to calculate Kurtosis (Y=1;N=0)
extern int CalcEcc; // Select whether one wants to calculate Eccentricities (Y=1;N=0)

// Probability Density Distributions
extern int LogNorm; // Select whether one wants to generate random numbers distribution using a lognormal pdf
extern int Gamma;  // Select whether one wants to generate random numbers distribution using a gamma pdf
extern int Weibull; // Select whether one wants to generate random numbers distribution using a weibull pdf
extern int BetaPrime; // Select whether one wants to generate random numbers distribution using a betaprime pdf
extern int Pareto; // Select whether one wants to generate random numbers distribution using a pareto pdf
extern int Nakagima; // Select whether one wants to generate random numbers distribution using a nakagima pdf

// Constants Quantities
const double inv_hbarc = 5.068; // 1/hbarc = 5.068 GeV-1 fm-1 // hbarc = 0.19733
const double g = sqrt(M_PI); // strong coupling constant alpha = g^2/(4pi)
const double Nc = 3.0; // number of colors
const double Qs0 = 1.24*inv_hbarc; // saturation scale taken from MAGMA (1.24 GeV) in fm-1
const double Qs0Ratio = 1.0; // Ratio between the saturation scale Qs0_1^2/Qs0_2^2

// Cutoffs
extern double m; // infrared cutoff taken from MAGMA (0.14 GeV) in fm-1  [r << 1/m]
extern double rdiv; // ultraviolet cutoff in fm  [dx >> rdiv] (optimal = 0.05)
extern double minMean; // Minimum value of energy density mean to be considered for covariance analysis (fm^-4)
                             // Reference: Mean at d = 6.4 fm is 52.92 fm-4;
//double Ldiv = log(4.0/pow(m*rdiv,2.0)); // -4pi del^2 L(0) = ln (4/m^2r0^2)
//double Ldiv = 1.0;
//double Ldiv = 4.0*rdiv*(log(2.0/(m*rdiv))+1.0);
extern double Ldiv;

// Impact Parameter
extern double b; // Impact parameter in fm (max = 0.7*grid/2.0)


gsl_vector *ThicknessFunction();
void OutputCholeskyinFile(gsl_matrix *p);
gsl_matrix *InputCholeskyinFile(long rows, long columns);
void OutputMeaninFile(gsl_vector *p);
gsl_vector *InputMeaninFile(long rows);
gsl_vector *EnergyDensityMeanInput(gsl_vector *Q);
gsl_matrix *CovarianceEnergyInput(gsl_vector *Q, int numselected, gsl_vector *iMeanPSelected);
gsl_vector *GetKWeinbull(gsl_vector *MeanPSelected, int numselected);

#endif // CHOLESKY_H_INCLUDED
