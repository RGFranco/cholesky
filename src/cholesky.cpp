#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>
#include <random>
#include <fstream>
#include "cholesky.h"
#include <string>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_eigen.h>

using namespace std;

// OutputCholeskyinFile is a function that prints out the value of Cholesky decomposition in a file
void OutputCholeskyinFile(gsl_matrix *A)
{
     FILE * f = fopen ("./data/cholesky.dat", "wb");
     gsl_matrix_fwrite (f, A);
     fclose (f);
}

// InputCholeskyinFile is a function that reads the value of the Cholesky decomposition in a file
gsl_matrix *InputCholeskyinFile(long rows, long columns)
{
    gsl_matrix *A = gsl_matrix_alloc(rows,columns);
    FILE * f = fopen ("./data/cholesky.dat", "rb");
    gsl_matrix_fread (f, A);
    fclose (f);
    return A;
}

// OutputMeaninFile is a function that prints out the value of the Mean in a file
void OutputMeaninFile(gsl_vector *A)
{
     FILE * f = fopen ("./data/mean.dat", "wb");
     gsl_vector_fwrite (f, A);
     fclose (f);
}

// InputMeaninFile is a function that reads the value of the Mean in a file
gsl_vector *InputMeaninFile(long rows)
{
    gsl_vector *A = gsl_vector_alloc(rows);
    FILE * f = fopen ("./data/mean.dat", "rb");
    gsl_vector_fread (f, A);
    fclose (f);
    return A;
}

// OutputVarianceinFile is a function that prints out the value of the Variance in a file
void OutputVarianceinFile(gsl_vector *A)
{
     FILE * f = fopen ("variance.dat", "wb");
     gsl_vector_fwrite (f, A);
     fclose (f);
}

// InputVarianceinFile is a function that reads the value of the Variance in a file
gsl_vector *InputVarianceinFile(long rows)
{
    gsl_vector *A = gsl_vector_alloc(rows);
    FILE * f = fopen ("variance.dat", "rb");
    gsl_vector_fread (f, A);
    fclose (f);
    return A;
}

gsl_vector *ThicknessFunction()
{
    gsl_vector *Q = gsl_vector_calloc(size+1);
    double chi = 0.546; // skin depth taken from Nuclear Charge-Density-Distribution parameters in fm
    double R0 = 6.62; // radius taken from MAGMA (6.62 fm) for 207Pb also from Nuclear Charge-Density...
    double lim = 15.0; // limits of the integral
    double sum = 0.0; double z = - lim;
    double n = 10001; // number of steps
    double h = 2.0*lim/(n-1); // step size
    for (int j = 0; j < n; j++)
    {
      sum = sum + 1.0/(1.0+exp((sqrt(z*z)-R0)/chi)) + 1.0/(1.0+exp((sqrt((z+h)*(z+h))-R0)/chi));
      z = z + h;
    }
    sum = sum*(h/2.0);
    double rho0 = 1.0/sum;
    for (int i = 0; i < size+1; i++)
    {
      double r = i*dx;
      sum = 0.0;
      z = -lim;
      for (int k = 0; k < n; k++)
      {
        sum = sum + rho0/(1.0+exp((sqrt(r*r+z*z)-R0)/chi)) + rho0/(1.0+exp((sqrt(r*r+(z+h)*(z+h))-R0)/chi));
        z = z + h;
      }
      sum = sum*(h/2.0);
      gsl_vector_set(Q,i,Qs0*Qs0*sum); //add *sum at the end
    }
    return Q;
}

gsl_vector *EnergyDensityMeanInput(gsl_vector *Q)
{
    gsl_vector *p = gsl_vector_alloc(numpoints);
    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, size+1);
    double ri[int(size+1)], Qi[int(size+1)];
    for (int i = 0; i < size+1; i++)
      {
        ri[i] = i*dx;
        Qi[i] = gsl_vector_get(Q,i);
      }
    gsl_spline_init (spline, ri, Qi, size+1);
    for (int i = 0; i < numpoints; i++)
    {
      double x = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
      double y = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
      double x1 = x - b/2.0;
      double x2 = x + b/2.0;
      double r1 = sqrt(x1*x1 + y*y);
      double r2 = sqrt(x2*x2 + y*y);
      double Qr1, Qr2;
      double Qmin = gsl_vector_get(Q,size);
      if (r1 > grid) Qr1 = Qmin;
      else Qr1 = gsl_spline_eval (spline, r1, acc);
      Qr1 = Qr1*Qs0Ratio;
      if (r2 > grid) Qr2 = Qmin;
      else Qr2 = gsl_spline_eval (spline, r2, acc);
      gsl_vector_set(p,i,((Nc*Nc-1.0)/(2.0*Nc*g*g))*Qr1*Qr2);
    }
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
    return p;
}

gsl_matrix *CovarianceEnergyInput(gsl_vector *Q, int numselected, gsl_vector *iMeanPSelected)
{
    gsl_matrix *A = gsl_matrix_alloc(numselected,numselected);
    double cutIR = pow(1.0/m,2.0); // Infrared Cutoff
    double cutUV = pow(0.01,2.0); // Ultraviolet Cutoff
    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, size+1);
    double ri[int(size+1)], Qi[int(size+1)];
    for (int i = 0; i < size+1; i++)
      {
        ri[i] = i*dx;
        Qi[i] = gsl_vector_get(Q,i);
      }
    gsl_spline_init (spline, ri, Qi, size+1);
    for (int iselect = 0; iselect < numselected; iselect++)
    {
        int i = gsl_vector_get(iMeanPSelected,iselect);
        double xi = (i%int(x_size+1) - x_size/2.0)*x_grid/x_size;
        double yi = (i/int(x_size+1) - y_size/2.0)*y_grid/y_size;
        double xib1 = xi - b/2.0;
        double xib2 = xi + b/2.0;
        double bi1 = sqrt(xib1*xib1 + yi*yi);
        double bi2 = sqrt(xib2*xib2 + yi*yi);
        double Qir1, Qir2;
        double Qmin = gsl_vector_get(Q,size);
        if (bi1 > grid) Qir1 = Qmin;
        else Qir1 = gsl_spline_eval (spline, bi1, acc);
        Qir1 = Qir1*Qs0Ratio;
        if (bi2 > grid) Qir2 = Qmin;
        else Qir2 = gsl_spline_eval (spline, bi2, acc);
        double Lr = 1.0;

        gsl_matrix_set(A,iselect,iselect,2.0*pow(Qir1*Qir2/(g*g),2.0)/3.0);

        for (int jselect = iselect+1; jselect < numselected; jselect++)
        {
            int j = gsl_vector_get(iMeanPSelected,jselect);
            double xj = (j%int(x_size+1) - x_size/2.0)*x_grid/x_size;
            double yj = (j/int(x_size+1) - y_size/2.0)*y_grid/y_size;
            double r2 = (xi-xj)*(xi-xj) + (yi-yj)*(yi-yj); // r2 = abs(r1 - r2)^2

            // // Qr1 = Qr1(b); Qr2 = Qr2(b); b = (xi+xj)/2
            // double xb = (xi+xj)/2.0; // x mean
            // double yb = (yi+yj)/2.0; // y mean
            // double xb1 = xb - b/2.0;
            // double xb2 = xb + b/2.0;
            // double b1 = sqrt(xb1*xb1 + yb*yb);
            // double b2 = sqrt(xb2*xb2 + yb*yb);
            // double Qr1, Qr2;
            // if (b1 > grid) Qr1 = gsl_vector_get(Q,size);
            // else Qr1 = gsl_spline_eval (spline, b1, acc);
            // Qr1 = Qr1*Qs0Ratio;
            // if (b2 > grid) Qr2 = gsl_vector_get(Q,size);
            // else Qr2 = gsl_spline_eval (spline, b2, acc);

            // Qr1 = Qr1(xi); Qr2 = Qr2(xj)
            double xjb1 = xj - b/2.0;
            double xjb2 = xj + b/2.0;
            double bj1 = sqrt(xjb1*xjb1 + yj*yj);
            double bj2 = sqrt(xjb2*xjb2 + yj*yj);
            double Qjr1, Qjr2;
            if (bj1 > grid) Qjr1 = Qmin;
            else Qjr1 = gsl_spline_eval (spline, bj1, acc);
            Qjr1 = Qjr1*Qs0Ratio;
            if (bj2 > grid) Qjr2 = Qmin;
            else Qjr2 = gsl_spline_eval (spline, bj2, acc);

            // geometric mean
            double Qr1 = sqrt(Qir1*Qjr1);
            double Qr2 = sqrt(Qir2*Qjr2);

            // // arithmetic mean
            // double Qr1 = (Qir1 + Qjr1)/2.0;
            // double Qr2 = (Qir2 + Qjr2)/2.0;

            // // Qr1 = Qr1(xi); Qr2 = Qr2(xj)
            // double Qr1 = Qir1;
            // double Qr2 = Qjr2;

            // // Qr1 = Qr1; Qr2 = Qr2; Constant
            // double Qr1 = Qs0*Qs0*Qs0Ratio;
            // double Qr2 = Qs0*Qs0;

            double A2 = pow((1.0/(8.0*M_PI))*log(4.0/(pow(m,2.0)*r2)),2.0);
            double B2 = pow(1.0/(4.0*M_PI),2.0);
            double RatioB2A2 = 4.0/pow(log(4.0/(pow(m,2.0)*r2)),2.0);
            Lr = log(4.0/(pow(m,2.0)*r2))/Ldiv;

            // GBW Limit (if m=0)
            if(!m){
              RatioB2A2 = 0.0;
              Lr = 1.0;
            }

            if (r2<cutIR)
            {
              // Covariance from ArXiV 1808.00795 (Albacete)

              double p1 = exp(-Qr1*Lr*r2/4.0)*(Qr1*Lr*r2 + 4.0) - 4.0;
              double p2 = exp(-Qr2*Lr*r2/4.0)*(Qr2*Lr*r2 + 4.0) - 4.0;
              double q1 = exp(-Qr1*Lr*r2/4.0)*(pow(Qr1*Lr*r2,2.0) + 8.0*Qr1*Lr*r2 + 32.0) - 32.0;
              double q2 = exp(-Qr2*Lr*r2/4.0)*(pow(Qr2*Lr*r2,2.0) + 8.0*Qr2*Lr*r2 + 32.0) - 32.0;

              double CovFull = (1.0/(pow(g*r2,4.0)*9.0))*(4.0*(16.0+pow(RatioB2A2,2.0))*p1*p2 + 2.0*q1*q2 - 2.0*(4.0-RatioB2A2)*(p1*q2+q1*p2) + 4.0*(4.0+RatioB2A2)*(pow(Qr1*r2,2.0)*(Qr2*Lr*r2 - 4.0 + 4.0*exp(-Qr2*Lr*r2/4.0)) + pow(Qr2*r2,2.0)*(Qr1*Lr*r2 - 4.0 + 4.0*exp(-Qr1*Lr*r2/4.0))) + (16.0 + 8.0*RatioB2A2 + pow(RatioB2A2,2.0))*(91.0/4.0 - (134.0/5.0)*(exp(-Qr1*Lr*r2/4.0)+exp(-Qr2*Lr*r2/4.0)) + (81.0/100.0)*exp(-2.0*Qr1*Lr*r2/3.0)*((3.0/2.0)*exp(-2.0*r2*Qr2*Lr/3.0) + 5.0 - 8.0*exp(-Qr2*Lr*r2/4.0)) + (81.0/100.0)*exp(-2.0*Qr2*Lr*r2/3.0)*((3.0/2.0)*exp(-2.0*r2*Qr1*Lr/3.0) + 5.0 - 8.0*exp(-Qr1*Lr*r2/4.0)) + Qr1*Lr*Qr2*Lr*r2*r2 - 4.0*r2*(Qr1*Lr*(1.0 - exp(-Qr2*Lr*r2/4.0)) + Qr2*Lr*(1.0 - exp(-Qr1*Lr*r2/4.0))) + (832.0/25.0)*exp(-(Qr1+Qr2)*Lr*r2/4.0)));

              gsl_matrix_set(A,iselect,jselect,CovFull);
            }
            if (r2 >= cutIR)
              gsl_matrix_set(A,iselect,jselect,0.0);

            gsl_matrix_set(A,jselect,iselect,gsl_matrix_get(A,iselect,jselect));
        }
    }
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
    return A;
}

gsl_vector *GetKWeinbull(gsl_vector *MeanPSelected, int numselected)
{
  double max = gsl_vector_get(MeanPSelected,0);
  double min = gsl_vector_get(MeanPSelected,0);
  double MeanX;
  for (int i=1; i<numselected; i++)
  {
    MeanX = gsl_vector_get(MeanPSelected,i);
    if (MeanX > max)
      max = MeanX;
    if (MeanX < min)
      min = MeanX;
  }
  gsl_vector *K = gsl_vector_alloc(numselected);
  int interp_size = 1000;
  double kmax = M_PI*max/sqrt(6);
  double kmin = 0.1;
  double dk = (kmax-kmin)/interp_size;
  gsl_interp_accel *acc = gsl_interp_accel_alloc();
  gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline, interp_size+1);
  double Ki[int(interp_size+1)], MeanXi[int(interp_size+1)];
  for (int i = 0; i < interp_size+1; i++)
  {
    Ki[i] = kmin + i*dk;
    MeanXi[i] = tgamma(1.0 + 1.0/Ki[i])/sqrt(tgamma(1.0 + 2.0/Ki[i])-pow(tgamma(1.0 + 1.0/Ki[i]),2.0));
  }
  gsl_spline_init (spline, MeanXi, Ki, interp_size+1);
  for (int i=0; i<numselected; i++)
    gsl_vector_set(K,i,gsl_spline_eval (spline, gsl_vector_get(MeanPSelected,i), acc));
  gsl_spline_free (spline);
  gsl_interp_accel_free (acc);
  return K;
}
